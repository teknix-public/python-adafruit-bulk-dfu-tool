import time
from multiprocess.pool import ThreadPool as Pool
import serial.tools.list_ports
import os
from time import sleep
import json
#################################################################################################################################################
#   ___       _          __               _  _    ______         _  _      _   _         _                    _   _____                _        #
#  / _ \     | |        / _|             (_)| |   | ___ \       | || |    | | | |       | |                  | | |_   _|              | |       #
# / /_\ \  __| |  __ _ | |_  _ __  _   _  _ | |_  | |_/ / _   _ | || | __ | | | | _ __  | |  ___    __ _   __| |   | |    ___    ___  | |       #
# |  _  | / _` | / _` ||  _|| '__|| | | || || __| | ___ \| | | || || |/ / | | | || '_ \ | | / _ \  / _` | / _` |   | |   / _ \  / _ \ | |       #
# | | | || (_| || (_| || |  | |   | |_| || || |_  | |_/ /| |_| || ||   <  | |_| || |_) || || (_) || (_| || (_| |   | |  | (_) || (_) || |       #
# \_| |_/ \__,_| \__,_||_|  |_|    \__,_||_| \__| \____/  \__,_||_||_|\_\  \___/ | .__/ |_| \___/  \__,_| \__,_|   \_/   \___/  \___/ |_|       #
#                                                                                | |                                                            #
#                                                                                |_|                                                            #
#################################################################################################################################################

# CHANGE THE pkg_dir BEFORE USE. ENSURE DEVICES ARE IN DFU MODE AND RUN THE SCRIPT
# some items in requirements.txt may not be required. Easier to install all of them in your venv if using pycharm.

pkg_dir = """C:\\PKG.zip""" #Directory of Teknix Provided Device Binaries / Firmware
name = "adafruit-nrfutil.exe" #DO NOT CHANGE - This script will find the nRFUtil.
path = "C:\\" #Change ONLY if NrfUtil not installed on C:\
nrfutil_dir = "" #DO NOT CHANGE

def setup(name, path): #Function to find the nrfUtil
    for root, dirs, files in os.walk(path):
        if name in files:
            directory = os.path.join(root, name)
            with open('location.json', 'w') as f:
                data = {"dir":str(directory)}
                json.dump(data, f)
            return os.path.join(root, name)

if os.path.exists('./location.json') is True: #checks if the setup function has already run and output to the location.json file.
    with open('location.json', 'r') as f:
        data = json.load(f)
        if 'dir' in data:
            try:
                print(data["dir"])
                nrfutil_dir = str(data["dir"])
            except:
                print('Error reading JSON File, but it exists.')
        else:
            try:
                nrfutil_dir = str(setup(name, path))
            except:
                print('Error Error writing file to directory:' + path)
else:
    try:
        nrfutil_dir = str(setup(name, path))
    except:
        print('Error Error writing file to directory:' + path)

def upload(port): #function to upload the firmware to multiple devices - DO NOT EDIT - mutithreading enabled.
    try:
        dowork = os.system(nrfutil_dir + " dfu serial -pkg " +pkg_dir + " -p "+ port)
        #print(dowork)
        time.sleep(1)
    except:
        print('error with item')

def worker(port):
    try:
        upload(port)
    except:
        print('error with item')


comlist = serial.tools.list_ports.comports()
connected = []

for element in comlist: #finds all connected USB Serial devices.
    connected.append(element.device)
pool_size = len(connected)
pool = Pool(pool_size)
sleep(3)

for item in connected:
        #print(item)
        pool.apply_async(worker, (item, ))

pool.close()
pool.join()
