Usage:
This tool allows a user to simultaneously upload firmware to Adafruit NRF52 devices using a simple python script. 
Make sure all the requirements are met. Download the firmware binaries to your C:\ root directory. 
Plug in any number of Adafruit devices. 
Run the script in PyCharm - it will automatically find all the connected devices and flash them at once. 
THE FIRMWARE ZIP MUST CONTAIN THE MANIFEST.JSON 


Required Software:
Python3 (https://www.python.org/downloads/)
Pip (https://pip.pypa.io/en/stable/installation/)
PyCharm (optional) (https://www.jetbrains.com/pycharm/download/)
Adafruit_nRF52_nrfutil (https://github.com/adafruit/Adafruit_nRF52_nrfutil/releases/tag/0.5.3.post17)


Python Libs from requirements.txt
> pip3 install -r requirements.txt